// Global Variables

// Array with all possible cities to be chosen
var cities = ['people', 'thing', 'child', 'government', 'woman', 'system', 'group', 'number', 'world', 'house', 'company', 'problem', 'service', 'place', 'party', 'school', 'country', 'point', 'member', 'state', 'family', 'month', 'business', 'night', 'question', 'information', 'power', 'change', 'interest', 'development', 'money', 'water', 'other', 'level', 'council', 'policy', 'market', 'court', 'effect', 'result', 'study', 'report', 'friend', 'authority', 'minister', 'office', 'right', 'mother', 'person', 'reason', 'period', 'centre', 'figure', 'society', 'police', 'community', 'million', 'price', 'control', 'action', 'issue', 'process', 'position', 'course', 'minute', 'education', 'research', 'subject', 'programme', 'moment', 'father', 'force', 'order', 'value', 'matter', 'health', 'decision', 'street', 'industry', 'patient', 'class', 'church', 'condition', 'paper', 'century', 'section', 'activity', 'hundred', 'table', 'death', 'building', 'sense', 'staff', 'experience', 'student', 'language', 'department', 'management', 'morning', 'committee', 'product', 'practice', 'evidence', 'ground', 'letter', 'meeting', 'union', 'event', 'support', 'range', 'stage', 'teacher', 'trade', 'voice', 'field', 'history', 'parent', 'account', 'material', 'situation', 'manager', 'project', 'record', 'example', 'training', 'window', 'difference', 'light', 'university', 'quality', 'pound', 'story', 'worker', 'model', 'nature', 'officer', 'structure', 'hospital', 'method', 'movement', 'detail', 'computer', 'amount', 'approach', 'award', 'president', 'scheme', 'chapter', 'theory', 'property', 'director', 'leader', 'south', 'application', 'board', 'production', 'secretary', 'chance', 'operation', 'opportunity', 'share', 'agreement', 'contract', 'picture', 'security', 'thousand', 'election', 'source', 'colour', 'future', 'animal', 'evening', 'benefit', 'heart', 'purpose', 'standard', 'doctor', 'factor', 'music', 'charge', 'pattern', 'design', 'piece', 'population', 'knowledge', 'performance', 'plant', 'pressure', 'environment', 'garden', 'analysis', 'success', 'thought', 'region', 'attention', 'relation', 'space', 'statement', 'demand', 'labour', 'principle', 'capital', 'choice', 'couple', 'hotel', 'player', 'station', 'village', 'association', 'attempt', 'feature', 'income', 'individual', 'effort', 'technology', 'difficulty', 'machine', 'degree', 'energy', 'growth', 'treatment', 'county', 'function', 'provision', 'sound', 'behaviour', 'defence', 'resource', 'floor', 'science', 'style', 'college', 'feeling', 'horse', 'response', 'skill', 'character', 'answer', 'economy', 'investment', 'brother', 'husband', 'argument', 'season', 'concern', 'element', 'glass', 'increase', 'claim', 'title', 'aspect', 'chairman', 'discussion', 'summer', 'daughter', 'customer', 'institution', 'river', 'profit', 'conference', 'division', 'measure', 'stone', 'commission', 'procedure', 'proposal', 'client', 'image', 'sector', 'attack', 'direction', 'attitude', 'disease', 'employment', 'affair', 'appeal', 'ability', 'campaign', 'holiday', 'medium', 'pupil', 'technique', 'version', 'advice', 'library', 'press', 'visit', 'advantage', 'surface', 'blood', 'culture', 'island', 'memory', 'return', 'television', 'variety', 'competition', 'extent', 'majority', 'parliament', 'speaker', 'access', 'cause', 'mouth', 'payment', 'trouble', 'context', 'facility', 'reference', 'second', 'survey', 'article', 'chair', 'earth', 'importance', 'object', 'agency', 'collection', 'possibility', 'public', 'document', 'sister', 'supply', 'budget', 'career', 'influence', 'solution', 'weight', 'damage', 'district', 'opinion', 'requirement', 'exchange', 'opposition', 'option', 'quarter', 'stock', 'whole', 'arrangement', 'concept', 'executive', 'match', 'network', 'occasion', 'radio', 'railway', 'target', 'corner', 'finger', 'forest', 'afternoon', 'crime', 'employee', 'equipment', 'kitchen', 'message', 'peace', 'review', 'scale', 'scene', 'speech', 'sport', 'strategy', 'expression', 'failure', 'partner', 'reader', 'shoulder', 'marriage', 'owner', 'trust', 'truth', 'newspaper', 'safety', 'sentence', 'start', 'trial', 'balance', 'branch', 'league', 'length', 'nation', 'accident', 'doubt', 'front', 'spirit', 'studio', 'train', 'contact', 'official', 'strength', 'debate', 'museum', 'reform', 'shape', 'transport', 'agent', 'artist', 'english', 'presence', 'protection', 'candidate', 'driver', 'master', 'meaning', 'queen', 'adult', 'consequence', 'exercise', 'assessment', 'beginning', 'proportion', 'route', 'speed', 'credit', 'impact', 'danger', 'flower', 'reaction', 'track', 'video', 'belief', 'comment', 'conclusion', 'content', 'distance', 'justice', 'estate', 'prison', 'reality', 'sight', 'winter', 'employer', 'objective', 'offer', 'vehicle', 'weekend', 'writer', 'battle', 'colleague', 'expert', 'farmer', 'injury', 'package', 'telephone', 'confidence', 'generation', 'insurance', 'painting', 'phone', 'sample', 'commitment', 'conflict', 'drink', 'judge', 'legislation', 'threat', 'visitor', 'volume', 'author', 'background', 'cabinet', 'engine', 'entry', 'manner', 'smile', 'stuff', 'victim', 'coffee', 'mountain', 'regulation', 'relief', 'category', 'consumer', 'dinner', 'exhibition', 'football', 'interview', 'tradition', 'traffic', 'appearance', 'bridge', 'contrast', 'description', 'discipline', 'existence', 'gentleman', 'housing', 'improvement', 'session', 'sheet', 'audience', 'crisis', 'prince', 'theatre', 'asset', 'explanation', 'flight', 'freedom', 'limit', 'magazine', 'pension', 'plate', 'respect', 'writing', 'capacity', 'challenge', 'dream', 'factory', 'finance', 'selection', 'spring', 'victory', 'while', 'youth', 'aircraft', 'decade', 'definition', 'examination', 'intention', 'notice', 'output', 'offence', 'reduction', 'address', 'appointment', 'bedroom', 'bottom', 'enterprise', 'middle', 'murder', 'weapon', 'absence', 'assembly', 'birth', 'bottle', 'criticism', 'error', 'instruction', 'module', 'settlement', 'store', 'teaching', 'transfer', 'channel', 'component', 'desire', 'grant', 'implication', 'institute', 'lunch', 'photograph', 'pleasure', 'recognition', 'republic', 'solicitor', 'temperature', 'waste', 'weather', 'block', 'brain', 'expenditure', 'experiment', 'guest', 'guide', 'household', 'nurse', 'program', 'publication', 'screen', 'silence', 'treaty', 'assumption', 'captain', 'congress', 'connection', 'cover', 'crowd', 'curriculum', 'initiative', 'journey', 'metal', 'noise', 'phase', 'scientist', 'search', 'sequence', 'violence', 'display', 'gallery', 'instrument', 'location', 'ministry', 'professor', 'reading', 'theme', 'combination', 'drive', 'faith', 'learning', 'opening', 'priority', 'prospect', 'soldier', 'tooth', 'troop', 'alternative', 'breath', 'castle', 'crown', 'literature', 'membership', 'mistake', 'motion', 'release', 'revenue', 'total', 'variation', 'border', 'criterion', 'incident', 'index', 'passage', 'pocket', 'suggestion', 'valley', 'winner', 'billion', 'deputy', 'device', 'engineering', 'foundation', 'fruit', 'religion', 'request', 'restaurant', 'specialist', 'square', 'surprise', 'chain', 'circle', 'creation', 'observation', 'present', 'strike', 'atmosphere', 'clause', 'coast', 'defendant', 'distinction', 'enemy', 'fashion', 'impression', 'leadership', 'marketing', 'mechanism', 'neighbour', 'panel', 'revolution', 'advance', 'beach', 'drawing', 'dress', 'engineer', 'liability', 'motor', 'negotiation', 'origin', 'potential', 'servant', 'ticket', 'welfare', 'cancer', 'champion', 'chief', 'citizen', 'convention', 'editor', 'expense', 'general', 'height', 'palace', 'prisoner', 'round', 'trend', 'vision', 'warning', 'achievement', 'being', 'comparison', 'cross', 'democracy', 'expectation', 'lawyer', 'lesson', 'living', 'notion', 'outcome', 'parish', 'signal', 'working', 'breakfast', 'charity', 'column', 'complaint', 'corporation', 'councillor', 'finding', 'grass', 'inflation', 'passenger', 'plane', 'plastic', 'score', 'shadow', 'shock', 'tenant', 'territory', 'touch', 'beauty', 'boundary', 'buyer', 'database', 'dispute', 'exception', 'formation', 'identity', 'inquiry', 'licence', 'politician', 'resident', 'resolution', 'struggle', 'supporter', 'topic', 'transaction', 'assistance', 'break', 'currency', 'emergency', 'fault', 'metre', 'minority', 'mirror', 'novel', 'phrase', 'pilot', 'preparation', 'proceeding', 'quantity', 'taste', 'tension', 'thinking', 'extension', 'involvement', 'partnership', 'pollution', 'premise', 'prize', 'spokesman', 'stress', 'tower', 'command', 'decline', 'delivery', 'depth', 'female', 'frame', 'framework', 'german', 'holder', 'obligation', 'protein', 'regime', 'saving', 'string', 'wheel', 'approval', 'bishop', 'chest', 'cottage', 'cycle', 'export', 'funding', 'governor', 'profession', 'protest', 'restriction', 'setting', 'stairs', 'travel', 'agriculture', 'autumn', 'average', 'camera', 'creature', 'critic', 'empire', 'focus', 'green', 'guard', 'habit', 'input', 'inspector', 'landscape', 'layer', 'plaintiff', 'purchase', 'recession', 'recovery', 'reputation', 'shareholder', 'silver', 'sleep', 'trading', 'arrival', 'bread', 'drama', 'efficiency', 'electricity', 'illness', 'judgment', 'laboratory', 'muscle', 'penalty', 'perspective', 'resistance', 'steel', 'sugar', 'uncle', 'abuse', 'alliance', 'angle', 'certificate', 'chancellor', 'cloud', 'coach', 'custom', 'dollar', 'expansion', 'lifespan', 'personality', 'philosophy', 'possession', 'producer', 'promotion', 'ratio', 'spending', 'symptom', 'variable', 'actor', 'awareness', 'chamber', 'cigarette', 'deposit', 'dozen', 'estimate', 'festival', 'final', 'frequency', 'honour', 'jacket', 'phenomenon', 'relative', 'reply', 'researcher', 'secret', 'shirt', 'tendency', 'wedding', 'american', 'apple', 'breach', 'cheek', 'dance', 'dealer', 'discovery', 'emotion', 'equation', 'french', 'furniture', 'instance', 'investor', 'landlord', 'mixture', 'promise', 'recording', 'retirement', 'routine', 'substance', 'throat', 'tourist', 'allowance', 'anger', 'birthday', 'carpet', 'consent', 'cricket', 'curtain', 'curve', 'darkness', 'disaster', 'enquiry', 'entrance', 'fight', 'guitar', 'hearing', 'infection', 'interaction', 'journal', 'judgement', 'knife', 'medicine', 'mortgage', 'paragraph', 'permission', 'platform', 'policeman', 'print', 'reserve', 'scope', 'shift', 'stream', 'witness', 'acquisition', 'adviser', 'airport', 'chemical', 'circuit', 'clock', 'consumption', 'cream', 'defeat', 'delay', 'edition', 'human', 'peasant', 'perception', 'personnel', 'priest', 'province', 'anxiety', 'consultant', 'count', 'fishing', 'formula', 'journalist', 'lecture', 'mission', 'occupation', 'percentage', 'remark', 'supplier', 'survival', 'watch', 'workshop', 'alcohol', 'barrier', 'climate', 'fortune', 'heaven', 'indication', 'measurement', 'observer', 'opponent', 'ownership', 'pitch', 'prayer', 'preference', 'smell', 'stomach', 'symbol', 'burden', 'catalogue', 'cheese', 'confusion', 'democrat', 'dimension', 'enthusiasm', 'evaluation', 'historian', 'interval', 'operator', 'princess', 'publisher', 'schedule', 'sheep', 'shell', 'storage', 'summary', 'talent', 'admission', 'breast', 'cheque', 'childhood', 'conduct', 'designer', 'fabric', 'import', 'label', 'leisure', 'lover', 'merchant', 'percent', 'poetry', 'proof', 'replacement', 'salary', 'shopping', 'smoke', 'squad', 'stake', 'steam', 'strain', 'tissue', 'tunnel', 'turnover', 'unity', 'vessel', 'addition', 'amendment', 'bathroom', 'brick', 'classroom', 'comfort', 'complex', 'conviction', 'corridor', 'draft', 'favour', 'grade', 'imagination', 'magistrate', 'mouse', 'movie', 'mystery', 'participant', 'profile', 'refugee', 'reward', 'sergeant', 'standing', 'surgery', 'tongue', 'vegetable', 'acceptance', 'architect', 'assistant', 'black', 'cathedral', 'ceiling', 'check', 'composition', 'deficit', 'departure', 'detective', 'discourse', 'excitement', 'gesture', 'limitation', 'local', 'pride', 'register', 'storm', 'summit', 'transition', 'treasury', 'universe', 'venture', 'weakness', 'album', 'assault', 'bench', 'button', 'canal', 'chart', 'clerk', 'coalition', 'concert', 'constraint', 'diary', 'expertise', 'fellow', 'glance', 'grammar', 'guardian', 'guideline', 'horror', 'infant', 'leather', 'margin', 'mummy', 'objection', 'opera', 'paint', 'passion', 'prosecution', 'psychology', 'reception', 'repair', 'shelf', 'stand', 'stick', 'timber', 'uncertainty', 'virtue', 'agenda', 'alarm', 'cable', 'carbon', 'charter', 'chicken', 'closure', 'companion', 'completion', 'cousin', 'craft', 'disorder', 'dividend', 'evolution', 'flesh', 'format', 'funeral', 'ideology', 'kingdom', 'lease', 'nerve', 'ocean', 'patch', 'portrait', 'potato', 'printer', 'privilege', 'punishment', 'rabbit', 'reflection', 'rival', 'specimen', 'taxation', 'traveller', 'volunteer', 'applicant', 'assurance', 'attraction', 'audit', 'borough', 'carriage', 'chocolate', 'commander', 'conversion', 'depression', 'destruction', 'discount', 'emperor', 'essay', 'exposure', 'favourite', 'friendship', 'garage', 'innovation', 'integration', 'junction', 'machinery', 'maker', 'organ', 'particle', 'petrol', 'planet', 'purchaser', 'remedy', 'resignation', 'seller', 'slope', 'strip', 'sympathy', 'terrace', 'vendor', 'wonder', 'allocation', 'ambition', 'calculation', 'chapel', 'collapse', 'conception', 'cotton', 'crash', 'darling', 'declaration', 'directive', 'disposal', 'excuse', 'federation', 'fibre', 'flame', 'grain', 'humour', 'hypothesis', 'incentive', 'inspection', 'interface', 'invitation', 'logic', 'mayor', 'mineral', 'molecule', 'mortality', 'needle', 'resort', 'rubbish', 'shade', 'stimulus', 'stranger', 'suspicion', 'trick', 'voter', 'withdrawal', 'youngster', 'accountant', 'angel', 'arrest', 'ceremony', 'cinema', 'clinic', 'cloth', 'competitor', 'complexity', 'constable', 'coverage', 'daddy', 'dictionary', 'disability', 'domain', 'equity', 'equivalent', 'explosion', 'fence', 'fragment', 'guarantee', 'insight', 'interior', 'leave', 'miner', 'processor', 'removal', 'running', 'satellite', 'server', 'stability', 'statute', 'tonne', 'adjustment', 'allegation', 'anniversary', 'banking', 'battery', 'bible', 'brand', 'butter', 'celebration', 'christian', 'clothing', 'colony', 'controversy', 'crystal', 'delight', 'desert', 'emission', 'episode', 'escape', 'fiction', 'fighting', 'fleet', 'hardware', 'insect', 'invasion', 'merger', 'minimum', 'monopoly', 'motive', 'necessity', 'nursery', 'photo', 'piano', 'rebel', 'rhythm', 'shortage', 'switch', 'temple', 'therapy', 'tournament', 'tribunal', 'trustee', 'villa', 'white', 'worth', 'wound', 'abbey', 'adventure', 'airline', 'attendance', 'businessman', 'champagne', 'cliff', 'colonel', 'compound', 'counter', 'creditor', 'devil', 'doctrine', 'essence', 'filter', 'flexibility', 'fraction', 'gender', 'ghost', 'harbour', 'heritage', 'hierarchy', 'indicator', 'landing', 'launch', 'leaflet', 'lorry', 'loyalty', 'outline', 'painter', 'pensioner', 'proposition', 'receiver', 'refusal', 'restoration', 'rumour', 'shore', 'singer', 'skirt', 'sociology', 'spectrum', 'successor', 'testing', 'theft', 'toilet', 'tragedy', 'triumph', 'uniform', 'verse', 'virus', 'warmth', 'widow', 'accent', 'analyst', 'apartment', 'appendix', 'avenue', 'bastard', 'breed', 'builder', 'bureau', 'capitalism', 'carrier', 'collector', 'compromise', 'continent', 'copper', 'crack', 'cupboard', 'defender', 'delegate', 'density', 'developer', 'diagnosis', 'dialogue', 'directory', 'discretion', 'doorway', 'duration', 'envelope', 'fantasy', 'fluid', 'forum', 'fraud', 'graduate', 'grave', 'heading', 'isolation', 'juice', 'killer', 'laugh', 'liberty', 'lifetime', 'merit', 'midnight', 'missile', 'musician', 'offender', 'oxygen', 'polytechnic', 'portfolio', 'premium', 'probability', 'receipt', 'recipe', 'reporter', 'residence', 'sandwich', 'sculpture', 'seminar', 'sensation', 'separation', 'shame', 'shower', 'speculation', 'stance', 'stroke', 'succession', 'suicide', 'sword', 'trace', 'truck', 'whisky', 'worry', 'ambulance', 'assignment', 'auditor', 'autonomy', 'basket', 'bonus', 'capability', 'clash', 'concession', 'correlation', 'delegation', 'diagram', 'divorce', 'eagle', 'easter', 'entity', 'finish', 'glory', 'guilt', 'holding', 'horizon', 'hunting', 'incidence', 'ingredient', 'instinct', 'liberation', 'motivation', 'nightmare', 'organism', 'packet', 'panic', 'pause', 'pavement', 'pregnancy', 'radiation', 'redundancy', 'reign', 'scandal', 'scholar', 'spell', 'sphere', 'spread', 'subsidiary', 'subsidy', 'swimming', 'tactic', 'teenager', 'thesis', 'timing', 'trader', 'trainer', 'tutor', 'accuracy', 'archbishop', 'blade', 'blanket', 'bronze', 'brush', 'casualty', 'constituent', 'contest', 'counterpart', 'diamond', 'dismissal', 'engagement', 'exclusion', 'execution'];

// bangkok - warsaw - brussels - venice - zurich - osaka

// Variable to store the chosen city to be played in each round
var cityInPlay = null;

// Array with each letter that forms the chosen city
var cityLetters = [];

// letter guessed by the user by pressing the keyboard buttons
var letterGuessed = null;

// Array with all wrong letters guessed by user
var guessedLetters = [];

// Array with all correct letters guessed by user
var matchedLetters = [];

// create a variable to store the cityInPLay word in a string
var wordView = "";

// Number of wrong guesses left
var guessesLeft = 10;

// Number of games the user won
var wins = 0;

// Number of games the user lost
var losses = 0;

$('#keyboard').hide();
$('#home-score').hide();
$(".btn-restart").hide();

// Start Screen Button
$(".btn-start").click(function(){
	$('#keyboard').show();
	$('#home-score').show();
	$('#start-button').hide();	
});

//call function to setup game
setupGame();

// Set Up and start game
function setupGame() {

	// Randomly choose 1 city from the cities array 
	// and store it in the cityInPlay variable
	cityInPlay = cities[Math.floor(Math.random() * cities.length)];
	
	// Split the letters from chosen city
	// and store in in the cityLetters Array
	cityLetters = cityInPlay.split('');
	
	// print the Wrong Guesses Left current no. to page
	$('#guesses').html(guessesLeft);

	// print number of wins
	$('#wins').html(wins);

	// print number of losses
	$('#losses').html(losses);


	// call the displayCity function to move the game on
	displayCity();

}

// function to register user letter keyboard clicks
function keyboardClick () {

	$('.btn-outline-primary').click(function () {

	    if (this.id == 'btn-a') {
	        letterGuessed = "a";
	        callLetter();
	        $(this).hide();
	    	} else if (this.id == 'btn-b') {        
	        letterGuessed = "b";
	        callLetter();
	        $(this).hide();
	    	} else if (this.id == 'btn-c') {        
	        letterGuessed = "c";
	        callLetter();
	        $(this).hide();
	    	} else if (this.id == 'btn-d') {        
	        letterGuessed = "d";
	        callLetter();
	        $(this).hide();
	    	} else if (this.id == 'btn-e') {        
	        letterGuessed = "e";
	        callLetter();
	        $(this).hide();
	    	} else if (this.id == 'btn-f') {        
	        letterGuessed = "f";
	        callLetter();
	        $(this).hide();
	    	} else if (this.id == 'btn-g') {        
	        letterGuessed = "g";
	        callLetter();
	        $(this).hide();
	    	} else if (this.id == 'btn-h') {        
	        letterGuessed = "h";
	        callLetter();
	        $(this).hide();
	    	} else if (this.id == 'btn-i') {        
	        letterGuessed = "i";
	        callLetter();
	        $(this).hide();
	    	} else if (this.id == 'btn-j') {        
	        letterGuessed = "j";
	        callLetter();
	        $(this).hide();
	    	} else if (this.id == 'btn-k') {        
	        letterGuessed = "k";
	        callLetter();
	        $(this).hide();
	    	} else if (this.id == 'btn-l') {        
	        letterGuessed = "l";
	        callLetter();
	        $(this).hide();
	    	} else if (this.id == 'btn-m') {        
	        letterGuessed = "m";
	        callLetter();
	        $(this).hide();
	    	} else if (this.id == 'btn-n') {        
	        letterGuessed = "n";
	        callLetter();
	        $(this).hide();
	    	} else if (this.id == 'btn-o') {        
	        letterGuessed = "o";
	        callLetter();
	        $(this).hide();
	    	} else if (this.id == 'btn-p') {        
	        letterGuessed = "p";
	        callLetter();
	        $(this).hide();
	    	} else if (this.id == 'btn-q') {        
	        letterGuessed = "q";
	        callLetter();
	        $(this).hide();
	    	} else if (this.id == 'btn-r') {        
	        letterGuessed = "r";
	        callLetter();
	        $(this).hide();
	    	} else if (this.id == 'btn-s') {        
	        letterGuessed = "s";
	        callLetter();
	        $(this).hide();
	    	} else if (this.id == 'btn-t') {        
	        letterGuessed = "t";
	        callLetter();
	        $(this).hide();
	    	} else if (this.id == 'btn-u') {        
	        letterGuessed = "u";
	        callLetter();
	        $(this).hide();
	    	} else if (this.id == 'btn-v') {        
	        letterGuessed = "v";
	        callLetter();
	        $(this).hide();
	    	} else if (this.id == 'btn-w') {        
	        letterGuessed = "w";
	        callLetter();
	        $(this).hide();
	    	} else if (this.id == 'btn-x') {        
	        letterGuessed = "x";
	        callLetter();
	        $(this).hide();
	    	} else if (this.id == 'btn-y') {        
	        letterGuessed = "y";
	        callLetter();
	        $(this).hide();
	    	} else if (this.id == 'btn-z') {        
	        letterGuessed = "z";
	        callLetter();
	        $(this).hide();
	    	}   	
	});

}

// call function to register letter keyboard clicks
keyboardClick();

// Display random city on page with dashes to be filled in
function displayCity () {

	// loop through the cityLetters array
	for (var i=0; i < cityLetters.length; i++){

			// if the cityLetter is in the matchedLetters array
			if (matchedLetters.indexOf(cityLetters[i]) != -1){
				// add the cityLetter to the wordView string
				wordView += cityLetters[i];			
			} else {
				// if not, print the dashes to be filled in
				wordView += '&nbsp;*&nbsp;';
			}
	}

	// print the vordView string variable to the page
	$('#city').html(wordView);

}

function callLetter () { 
	// if the letter guessed by user is right
	// and NOT already in the matched letters array
	if ((cityLetters.indexOf(letterGuessed) != -1) && (matchedLetters.indexOf(letterGuessed) == -1)){ 

		// call the function to update right guesses
		correctUpdate();

	} else {
		// if not, call the function to update wrong guesses
		wrongUpdate();

	}

}

// Update the city display on page
function displayUpdate () {

			// set the wordView to display the updates
			var wordView = "";

			// loop through the cityLetters array
			for (var i=0; i < cityLetters.length; i++){

			// if the cityLetter is in the matchedLetters array
			if (matchedLetters.indexOf(cityLetters[i]) != -1){
				// add the cityLetter to the wordView string
				wordView += cityLetters[i];			
			} else {
				// if not, print the dashes to be filled in
				wordView += '&nbsp;*&nbsp;';
			}
		}

		// print the updated wordView string to the page
		$('#city').html(wordView);

}

// Update correct guesses
function correctUpdate () {

	// loop through the array of letters of the City in play
	for (var i = 0; i < cityLetters.length; i++) {

			// if the letter guessed by the user
			// is equal to any of the city Letters
			// and is NOT already in the matched letters array
			if ((letterGuessed === cityLetters[i]) && (matchedLetters.indexOf(letterGuessed) == -1)){

				// insert the letter into the matched letters array
				matchedLetters.push(letterGuessed);}

				// checking if there are duplicate letters in word
			else if ((letterGuessed === cityLetters[i + 1])) {

				matchedLetters.push(letterGuessed);
			};
	}

	// call the function to update the city display on page
	displayUpdate();

	// End game if user gets the word right (win) or runs out of guesses (loss)
	if (guessesLeft == 0) {
			youLose();
	 } 	else if (matchedLetters.length == cityLetters.length) {
			youWin();
	}

}

// Update Wrong Guesses
function wrongUpdate () {

	// if the letter guessed by the user
	// is NOT in the already guessed letters array
	// and is a wrong guess - not part of the city word
	if ((guessedLetters.indexOf(letterGuessed) == -1) && (cityLetters.indexOf(letterGuessed) == -1)){

		// decrease the numer of guesses left by one
		guessesLeft--;

		// put the letter in the already guessed array
		guessedLetters.push(letterGuessed);

		// print the updated no. of guesses left to page
		$('#guesses').html(guessesLeft);

		// print the already guessed letter to page
		$('#letters-guessed').html(guessedLetters.join(' - ').toUpperCase());
	}


	// End game if user gets the word right (win) or runs out of guesses (loss)
	if (guessesLeft == 0) {
			youLose();
	 } 	else if (matchedLetters.length == cityLetters.length) {
			youWin();
	}

}


// Function to end game when user wins
function youWin () {

	// increase the Wins score by one
	wins++;

	// Update the Wins score on the page
	$('#wins').html(wins);

	// Hide Keyboard
	$('#keyboard').hide();
	$('#guesses-home').hide();

	// print the "YOU WIN" phrase to the page
	$('#news').html("<h3><font color='blue'>CONGRATS, YOU WIN!</font></h3>");

	// print the city image to the page
	//$('#picture').html('<img src="assets/images/' + cityInPlay + '.jpg" width="95%" height="95%">');
	$('#picture').html('<img src="assets/images/win.jpg" width="95%" height="95%">');

	// Show the Restart button and call Restart function on click
	$(".btn-restart").show();

	$(".btn-restart").click(function(){
		restartGame();
	});	

}

// Function to end the game when user loses 
function youLose() {

	// increase the Loss score by one
	losses++;

	// Update the Loss score on the page
	$('#losses').html(losses);

	// Hide Keyboard
	$('#keyboard').hide();
	$('#guesses-home').hide();

	// print the "YOU LOSE" phrase to the page
	$('#news').html("<h3><font color='red'>"+cityInPlay+"</font></h3>YOU LOSE, TRY AGAIN!");
	
	// Show the Restart button and call Restart function on click
	$(".btn-restart").show();

	$(".btn-restart").click(function(){
		restartGame();
	});	
}

// restart the game so users can keep playing
function restartGame() {

	// reset game variables
	wordView = "";
	cityInPlay = null;
	cityLetters.length = 0;
	matchedLetters.length = 0;
	guessedLetters.length = 0;
	guessesLeft = 10;
	letterGuessed = null;
	wins = wins;
	losses = losses;

	$(".btn-restart").hide();
	$('#keyboard').show();
	$(".btn-outline-primary").show();
	$('#guesses-home').show();

	// reset the already guessed letters sectio
	$('#letters-guessed').html(guessedLetters.join(' - ').toUpperCase());

	// reset the Wrong Guesses Left section
	$('#guesses').html(guessesLeft);

	// print the current number of wins
	$('#wins').html(wins);

	// // print current number of losses
	$('#losses').html(losses);

	// reset the News div
	$('#news').html("");

	// reset the Picture div
	$('#picture').html("");

	// call function to start game again
	setupGame();

}